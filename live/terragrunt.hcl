remote_state {
  backend = "s3"
  config = {
    bucket         = "terragrunt-gitlab-runner-backend"
    key            = "gitlab-runner/${path_relative_to_include()}/terraform.tfstate"
    region         = "eu-central-1"
    encrypt        = true
    dynamodb_table = "terragrunt-gitlab-runner-backend"
    profile        = "polar-squad"
  }
}
