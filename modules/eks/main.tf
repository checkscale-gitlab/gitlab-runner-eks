terraform {
  backend "s3" {}
  required_version = "~> 0.13"
}

provider "aws" {
  region  = var.aws_region
  version = "= 3.11.0"
  profile = var.profile
}

data "aws_vpc" "selected" {
  id = var.vpc_id
}

data "aws_subnet_ids" "internal-selected" {
  vpc_id = data.aws_vpc.selected.id

  tags = {
    "kubernetes.io/role/internal-elb" = 1
  }
}

data "aws_subnet_ids" "public-selected" {
  vpc_id = data.aws_vpc.selected.id

  tags = {
    "kubernetes.io/role/elb" = 1
  }
}
data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  load_config_file       = false
  version                = "~> 1.13"
}

resource "aws_kms_key" "eks" {
  description = "EKS Secret Encryption Key ${var.environment}"
  tags = {
    name = var.environment
    Terraform   = "true"
    Environment = var.environment
  }
}
//noinspection MissingModule
module "eks" {
  source                                     = "terraform-aws-modules/eks/aws"
  version                                    = "13.0.0"
  cluster_name                               = "eks-${var.environment}-cluster"
  cluster_version                            = "1.18"
  subnets                                    = concat(tolist(data.aws_subnet_ids.internal-selected.ids), tolist(data.aws_subnet_ids.public-selected.ids))
  kubeconfig_aws_authenticator_env_variables = { AWS_PROFILE = var.profile }
  cluster_enabled_log_types                  = ["api","audit","authenticator","controllerManager","scheduler"]
  cluster_log_retention_in_days              = "30"
  enable_irsa                                = true
  tags = {
    Terraform   = "true"
    Environment = var.environment
  }

  vpc_id    = var.vpc_id
  map_roles = var.fargate_enable == true ? [
    {
      rolearn  = aws_iam_role.eks-fargate-profile[0].arn
      username = "system:node:{{SessionName}}"
      groups   = ["system:bootstrappers","system:nodes","system:node-proxier"]
    }
  ] : []
  cluster_encryption_config = [
    {
      provider_key_arn = aws_kms_key.eks.arn
      resources        = ["secrets"]
    }
  ]

  node_groups = {
    demand = {
      desired_capacity = 3
      max_capacity     = 10
      min_capacity     = 3
      instance_type = "m5.large"
      subnets = tolist(data.aws_subnet_ids.internal-selected.ids)
    }
  }
}

resource "aws_iam_role" "eks-fargate-profile" {
  count = var.fargate_enable == true ? 1 : 0
  name = "eks-fargate-profile-${var.environment}"

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "eks-fargate-pods.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })

}

resource "aws_iam_role_policy_attachment" "eks-fargate-AmazonEKSFargatePodExecutionRolePolicy" {
  count = var.fargate_enable == true ? 1 : 0
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSFargatePodExecutionRolePolicy"
  role       = aws_iam_role.eks-fargate-profile[0].name
}

resource "aws_eks_fargate_profile" "eks-fargate-profile-gitlab-runner" {
  depends_on = [module.eks]
  count = var.fargate_enable == true ? 1 : 0
  cluster_name           = "eks-${var.environment}-cluster"
  fargate_profile_name   = "gitlab-runner"
  pod_execution_role_arn = aws_iam_role.eks-fargate-profile[0].arn
  subnet_ids             = tolist(data.aws_subnet_ids.internal-selected.ids)

  selector {
    namespace = "gitlab-runner"
  }
  tags = {
    Terraform   = "true"
    Environment = var.environment
  }
}

//noinspection MissingModule
module "iam_assumable_role_admin" {
  source                        = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version                       = "~> v3.3.0"
  create_role                   = true
  role_name                     = var.k8s_autosaler_service_account_name
  provider_url                  = replace(module.eks.cluster_oidc_issuer_url, "https://", "")
  role_policy_arns              = [aws_iam_policy.cluster_autoscaler.arn]
  number_of_role_policy_arns    = 1
  oidc_fully_qualified_subjects = ["system:serviceaccount:kube-system:${var.k8s_autosaler_service_account_name}"]
}

resource "aws_iam_policy" "cluster_autoscaler" {
  name_prefix = "cluster-autoscaler"
  description = "EKS cluster-autoscaler policy for cluster ${module.eks.cluster_id}"
  policy      = data.aws_iam_policy_document.cluster_autoscaler.json
}

data "aws_iam_policy_document" "cluster_autoscaler" {
  statement {
    sid    = "clusterAutoscalerAll"
    effect = "Allow"

    actions = [
      "autoscaling:DescribeAutoScalingGroups",
      "autoscaling:DescribeAutoScalingInstances",
      "autoscaling:DescribeLaunchConfigurations",
      "autoscaling:DescribeTags",
      "ec2:DescribeLaunchTemplateVersions",
    ]

    resources = ["*"]
  }

  statement {
    sid    = "clusterAutoscalerOwn"
    effect = "Allow"

    actions = [
      "autoscaling:SetDesiredCapacity",
      "autoscaling:TerminateInstanceInAutoScalingGroup",
      "autoscaling:UpdateAutoScalingGroup",
    ]

    resources = ["*"]

    condition {
      test     = "StringEquals"
      variable = "autoscaling:ResourceTag/kubernetes.io/cluster/${module.eks.cluster_id}"
      values   = ["owned"]
    }

    condition {
      test     = "StringEquals"
      variable = "autoscaling:ResourceTag/k8s.io/cluster-autoscaler/enabled"
      values   = ["true"]
    }
  }
}

resource "kubernetes_namespace" "develop" {
  depends_on = [module.eks]
  metadata {
    annotations = {
      name = "develop"
    }
    name = "develop"
  }
}

resource "kubernetes_namespace" "prod" {
  depends_on = [module.eks]
  metadata {
    annotations = {
      name = "prod"
    }
    name = "prod"
  }
}




